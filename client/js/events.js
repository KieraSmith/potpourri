"use strict";

Template.form.events({
    'submit form': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[3].children[0].children[1].files[0];

        var author = event.currentTarget.children[0].children[0].children[0].value;

        var message = event.currentTarget.children[1].children[0].children[0].value;

        if (imageFile === undefined) {
            Materialize.toast('Insert an Image', 4000);
            return false;
        } else if (author === "") {
            Materialize.toast('Name Please', 4000);
            return false;
        } else if (message === "") {
            Materialize.toast('Write a Message', 4000);
            return false;
        }


        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                Materialize.toast('Somethings wrong...try again', 4000);
                return false;

            } else {
                Collections.Post.insert({
                    name: author,
                    createdAt: new Date(),
                    message: message,
                    ImageID: fileObject._id,
                    username: Meteor.user().username
                });

                event.currentTarget.children[3].children[0].children[1].files[0] = undefined;

                event.currentTarget.children[0].children[0].children[0].value = "";

                event.currentTarget.children[1].children[0].children[0].value = "";
                //submit post data to database 
                //Author, Message, created by date, ImageID
                //fileObject._id
                $('.grid').masonry('reloadItems');
            }
        });
    }
});

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});